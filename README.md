# CI/CD Masterclass

Bienvenue dans le projet CI/CD Masterclass ! Ce projet a été créé dans le cadre de mon cours sur CI/CD pour explorer et mettre en pratique les principes du Continuous Integration (CI) et Continuous Deployment (CD) avec GitLab.

## Objectif du Projet

L'objectif principal de ce projet est de fournir un environnement d'apprentissage pratique pour comprendre les concepts et les outils liés à CI/CD. Nous explorerons les pipelines, les jobs, et d'autres fonctionnalités de GitLab pour automatiser le processus de développement logiciel.

## Structure du Projet

Le projet est organisé comme suit :

- **src**: Le répertoire source contient le code de l'application.
- **tests**: Les tests automatisés sont stockés ici.
- **.gitlab-ci.yml**: Le fichier de configuration des pipelines GitLab.

## Comment Commencer

1. Clonez ce référentiel sur votre machine locale.
   ```bash
   git clone [lien du référentiel]
