FROM liliancal/ubuntu-php-apache
COPY src/ /var/www/
RUN apt-get update -y && \
    apt-get install -y libapache2-mod-php7.4 libgs9-common libgs9 && \
    apt-get clean && rm -rf /var/lib/apt/lists/*
EXPOSE 80
CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
